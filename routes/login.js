var express = require('express');
var router = express.Router();
var passport = require('passport');
var Account = require('../models/account');

router.post('/', function(req, res) {
	Account.findOne({ username: req.body.username }, function(err, user){
		if(err){
			throw err;
		}
		if(!user){
			res.json(0);
		}
		else{
			passport.authenticate('local')(req, res, function(){
				res.json(req.session.passport.user);
			});
		}
	});
});

module.exports = router;
