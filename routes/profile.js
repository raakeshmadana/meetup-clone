var express = require('express');
var router = express.Router();
var monk = require('monk');

const db = monk('wpl:wolverine@ds121686.mlab.com:21686/meetup');

router.post('/create', function(req, res){
	if(req.session){
		var collection = db.get('members');
		req.body._id = req.session.passport.user;
		collection.insert(req.body, {castIds: false}, function(err, data){
			if(err){
				res.json(0);
			}
			else{
				res.json(1);
			}
		});
	}
});

router.get('/view', function(req, res){
	if(req.session){
		var collection = db.get('members');
		collection.findOne({ _id: req.session.passport.user }, { castIds: false }, function(err, member){
			if(err){
				res.json(0);
			}
			else{
				res.json(member);
			}
		});
	}
});

router.post('/edit', function(req, res){
	if(req.session){
		var collection = db.get('members');
		collection.update({ _id: req.session.passport.user }, req.body, { castIds: false }, function(err){
			if(err){
				res.json(0);
			}
			else{
				res.json(1);
			}
		});
	}
});

module.exports = router;
