var express = require('express');
var router = express.Router();
var monk = require('monk');
var ObjectID = require('mongodb').ObjectID;
//const safeObjectId = s => ObjectId.isValid(s) ? new ObjectId(s): null;

var db = monk('wpl:wolverine@ds121686.mlab.com:21686/meetup');

router.post('/create', function(req, res){
	if(req.session){
		var meetings = db.get('meetings');
		req.body.host = req.session.passport.user;
		req.body.going.push(req.session.passport.user);
		meetings.insert(req.body, function(err, insertedDoc){
			if(err){
				res.json(0);
			}
			else{
				var members = db.get('members');
				members.update({ _id: req.session.passport.user }, { $push: { hosted: insertedDoc._id.toString() } }, { castIds: false }, function(err){		
					if(err){
						res.json(0);
					}
					else{
						res.json(1);
					}
				});
			}
		});
	}
});

router.post('/profile', function(req, res){
	var meetups = db.get('meetings');
	var objectId = new ObjectID(req.body.meetupId);
	meetups.findOne({_id: objectId}, function(err, meetup){
		if(err){
			throw err;
		}
		else{
			res.json(meetup);
		}
	});
});

router.get('/show', function(req, res){
	if(req.session){
		var members = db.get('members');
		members.findOne({ _id: req.session.passport.user }, { castIds:false }, function(err, member){
			var collection = db.get('meetings');
			collection.find({
				$or: [
					{ 'location.city': { $regex: member.location.city, $options: 'i'} },
					{ 'location.state': { $regex: member.location.state, $options:  'i'} },
					{ category: { $in: member.interests } }
				]
				},
				function(err, meetups){
					if(err){
						throw err;
					}
					else{
						res.json(meetups);
					}
				});
		});
	}
});

//Display all meetups hosted by a user
/*router.get('/profile', function(req, res){
	if(req.session){
		var members = db.get('members');
		members.findOne({ _id: req.session.passport.user }, { castIds: false }, function(err, member){
			if(err){
				throw err;
			}
			else{
				var meetups = db.get('meetings');
				var hostedMeetups = [];
				for(i = 0; i <  member.hosted.length; i++){
					var objectId = new ObjectID(member.hosted[i]);
					meetups.findOne({_id: objectId}, function(err, meetup){
						if(err){
							throw err;
						}
						else{
							hostedMeetups.push(meetup);
						}
					});
				}
				res.json(hostedMeetups);
			}
		});
	}
});*/

router.get('/:id', function(req, res){
	var meetups = db.get('meetings');
	meetups.findOne({_id: req.params.id}, function(err, meetup){
		if(err){
			throw err;
		}
		else{
			res.json(meetup);
		}
	});
});

router.post('/rsvp', function(req, res){
	var meetups = db.get('meetings');
	meetups.findOneAndUpdate({_id: req.body.id}, { $push: { going: req.session.passport.user } }, function(err, updatedDoc){
		if(err){
			console.log(err);
			throw err;
		}
		else{
			console.log(updatedDoc);
			res.json(updatedDoc);
		}
	});
});

router.get('/search', function(req, res){
});

module.exports = router;
