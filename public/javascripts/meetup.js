var app = angular.module('meetup-clone', ['ngResource', 'ngRoute', 'checklist-model']);

app.config(['$routeProvider', function($routeProvider){
	$routeProvider
		.when('/', {
			templateUrl: 'partials/index.html',
		})
		.when('/login', {
			templateUrl: 'partials/login.html',
			controller: 'LoginCtrl'
		})
		.when('/home', {
			templateUrl: 'partials/home.html',
			controller: 'HomeCtrl'
		})
		.when('/register', {
			templateUrl: 'partials/register.html',
			controller: 'RegisterCtrl'
		})
		.when('/createProfile', {
			templateUrl: 'partials/createProfile.html',
			controller: 'CreateProfileCtrl'
		})
		.when('/viewProfile', {
			templateUrl: 'partials/viewProfile.html',
			controller: 'ViewProfileCtrl'
		})
		.when('/editProfile', {
			templateUrl: 'partials/editProfile.html',
			controller: 'EditProfileCtrl'
		})
		.when('/createMeetup', {
			templateUrl: 'partials/createMeetup.html',
			controller: 'CreateMeetupCtrl'
		})
		.when('/viewMeetup/:id', {
			templateUrl: 'partials/viewMeetup.html',
			controller: 'ViewMeetupCtrl'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);

app.controller('HomeCtrl', ['$scope', '$http', '$rootScope', '$location',
	function($scope, $http, $rootScope, $location){
		$http.get('/meetup/show').
			success(function(meetups){
				$scope.meetups = meetups;
			});
	}]);

app.controller('RegisterCtrl', ['$scope', '$http', '$rootScope', '$location',
	function($scope, $http, $rootScope, $location){
		$scope.register = function(isValid){
			if(isValid){
				$http.post('/register', $scope.user)
					.success(function(data){
						if(data){
							$rootScope.currentUser = data;
							$location.path('/createProfile');
						}
						else{
							alert("A user with that email already exists");
						}
					});
			}
		};
	}]);

app.controller('CreateProfileCtrl', ['$scope', '$http', '$rootScope', '$location',
	function($scope, $http, $rootScope, $location){
		$scope.interests = [
			'Arts',
			'Auto',
			'Book Clubs',
			'Business',
			'Community',
			'Crafts',
			'Cult',
			'Dancing',
			'Education',
			'Family',
			'Fashion',
			'Films',
			'Fitness',
			'Food & Drink',
			'Games',
			'Movements',
			'Health',
			'Languages',
			'LGBTQ',
			'Lifestyle',
			'Music',
			'Outdoors',
			'Paranormal',
			'Pets',
			'Photography',
			'Sci-fi',
			'Singles',
			'Social',
			'Spirituality',
			'Sports',
			'Support',
			'Tech',
			'Writing'
		];
		$scope.profile = {
			interests: ['Arts']
		};

		$scope.createProfile = function(isValid){
			if(isValid){
				var params = {
					_id: null,
					name: $scope.profile.name,
					location: {
						city: $scope.profile.city,
						state: $scope.profile.state
					},
					interests: $scope.profile.interests,
					rsvp: [],
					notattended: [],
					hosted: []
				};
				$http.post('/profile/create', params)
					.success(function(data){
						if(data){
							$location.path('/home');
						}
					});
			}
		}
	}]);

app.controller('ViewProfileCtrl', ['$scope', '$http', '$rootScope', '$location',
	function($scope, $http, $rootScope, $location){
		$http.get('/profile/view').
			success(function(member){
				$scope.member = member;
				$scope.hostedMeetups = [];
				for(var i = 0; i < $scope.member.hosted.length; i++){
					var param = {
						meetupId: $scope.member.hosted[i]
					};
					$http.post('/meetup/profile', param).
						success(function(meetup){
							$scope.hostedMeetups.push(meetup);
						});
				}
			});
		/*$scope.hostedMeetups = [];
		for(var i = 0; i < $scope.member.hosted.length; i++){
			var param = {
				meetupId: $scope.member.hosted[i]
			};
			$http.post('/meetup/profile', param).
				success(function(meetup){
					$scope.hostedMeetups.push(meetup);
				});
		}*/
		/*$http.get('/meetup/profile').
			success(function(meetups){
				$scope.hostedMeetups = meetups;
			});
				*/
	}]);

app.controller('EditProfileCtrl', ['$scope', '$http', '$rootScope', '$location',
	function($scope, $http, $rootScope, $location){
		$http.get('/profile/view').
			success(function(member){
				$scope.profile = member;
			});
		$scope.interests = [
			'Arts',
			'Auto',
			'Book Clubs',
			'Business',
			'Community',
			'Crafts',
			'Cult',
			'Dancing',
			'Education',
			'Family',
			'Fashion',
			'Films',
			'Fitness',
			'Food & Drink',
			'Games',
			'Movements',
			'Health',
			'Languages',
			'LGBTQ',
			'Lifestyle',
			'Music',
			'Outdoors',
			'Paranormal',
			'Pets',
			'Photography',
			'Sci-fi',
			'Singles',
			'Social',
			'Spirituality',
			'Sports',
			'Support',
			'Tech',
			'Writing'
		];
		$scope.editProfile = function(isValid){
			if(isValid){
				var tempprofile = $scope.profile;
				delete tempprofile._id;
				$http.post('/profile/edit', tempprofile).
					success(function(data){
						alert("Changes saved");
						$location.path('/home');
					});
			}
		};
	}]);

app.controller('LoginCtrl', ['$scope', '$http', '$rootScope', '$location', 
	function($scope, $http, $rootScope, $location){
		$scope.login = function(isValid){
			if(isValid){
				$http.post('/login', $scope.user).
					success(function(user){
						if(user == 0){
							alert("User with that email has not registered yet");
						}
						else{
							$rootScope.currentUser = user;
							$location.path('/home');
						}
					}).
					error(function(data, status, headers, config){
						if(status === 401){
							alert("Wrong password");
						}
					});
			}
		};
	}]);

app.controller('CreateMeetupCtrl', ['$scope', '$http', '$rootScope', '$location',
	function($scope, $http, $rootScope, $location){
		$scope.categories = [
			'Arts',
			'Auto',
			'Book Clubs',
			'Business',
			'Community',
			'Crafts',
			'Cult',
			'Dancing',
			'Education',
			'Family',
			'Fashion',
			'Films',
			'Fitness',
			'Food & Drink',
			'Games',
			'Movements',
			'Health',
			'Languages',
			'LGBTQ',
			'Lifestyle',
			'Music',
			'Outdoors',
			'Paranormal',
			'Pets',
			'Photography',
			'Sci-fi',
			'Singles',
			'Social',
			'Spirituality',
			'Sports',
			'Support',
			'Tech',
			'Writing'
		];
		$scope.createMeetup = function(isValid){
			var params = {
				title: $scope.meetup.title,
				host: null,
				category: $scope.meetup.category,
				date: $scope.meetup.date,
				time: {
					from: $scope.meetup.from,
					to: $scope.meetup.to
				},
				details: $scope.meetup.details,
				location: {
					street: $scope.meetup.street,
					city: $scope.meetup.city,
					state: $scope.meetup.state
				},
				slots: $scope.meetup.slots,
				going: [],
				notgoing: []
			};

			if(isValid){
				$http.post('/meetup/create', params).
					success(function(data){
						alert('Meetup created');
						$location.path('/home');
					});
			}
		};
	}]);

app.controller('ViewMeetupCtrl', ['$scope', '$http', '$resource', '$location', '$routeParams',
	function($scope, $http, $resource , $location, $routeParams){
		var meetups = $resource('/meetup/:id', { id: '@_id' }, {
			update: { method: 'PUT' }
		});

		meetups.get({ id: $routeParams.id }, function(meetup){
			$scope.meetup = meetup;
		});

		$scope.rsvp = function(){
			if($scope.meetup.going.length < $scope.meetup.slots){
				var params = {
					id: $scope.meetup._id
				};
				$http.post('/meetup/rsvp', params).
				   success(function(meetup){   
					   alert('RSVPed successfully');
					   $scope.meetup = meetup;
				   });
			}
			else{
				alert('No more slots left!');
			}
		};
	}]);
